using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace StringUtilities
{
    public static class StringUtils
    {
        public static string SplitCamelCase(string input)
        {
            return Regex
                    .Replace(input,
                             "(?<=[a-z])([A-Z])",
                             " $1",
                             System.Text.RegularExpressions.RegexOptions.Compiled)
                    .Trim();
        }

        /// <summary>
        /// Use Python-esque string slicing.
        /// </summary>
        public static string Slice(this string currentString, int startIndex)
        {
            int sliceIndex = startIndex;

            if (startIndex < 0) // if we get a negative
            {
                sliceIndex = currentString.Length + startIndex;
            }

            return currentString.Substring(sliceIndex);
        }

        /// <summary>
        /// Use Python-esque string slicing.  Returns "" instead of throwing an excption
        /// </summary>
        public static string SliceSafe(this string currentString, int startIndex)
        {
            try
            {
                return Slice(currentString, startIndex);
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Use Python-esque string slicing.
        /// </summary>
        public static string Slice(this string currentString, int startIndex, int endIndexNonInclusive)
        {
            string slicedString = currentString.Slice(startIndex);

            int charsUntilEndIndex = currentString.Length - endIndexNonInclusive + 1;

            if (endIndexNonInclusive < 0)
            {
                if (startIndex < endIndexNonInclusive)
                {
                    charsUntilEndIndex = slicedString.Length - Math.Abs(endIndexNonInclusive);
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }

            return slicedString.Substring(0, charsUntilEndIndex);
        }

        /// <summary>
        /// Use Python-esque string slicing.  Returns "" instead of throwing an excption
        /// </summary>
        public static string SliceSafe(this string currentString, int startIndex, int endIndexNonInclusive)
        {
            try
            {
                return Slice(currentString, startIndex, endIndexNonInclusive);
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Returns a multiline String with no leading or trailing whitespace for each line.
        /// 
        /// </summary>
        /// <param name="currentString"></param>
        /// <returns></returns>
        public static string MultiLineTrim(this string currentString, String desiredLineEnds)
        {
            string trimmedString = currentString;
            
            if (!String.IsNullOrWhiteSpace(currentString))
            {
                trimmedString = currentString.NormalizeLineEndings(desiredLineEnds);
                StringBuilder currentLineStringBuilder = new StringBuilder();
                
                String[] currentStringLines =
                    trimmedString.Split(new string[] { desiredLineEnds },
                                        StringSplitOptions.RemoveEmptyEntries);

                foreach (String currentLine in currentStringLines)
                {
                    currentLineStringBuilder.Append(currentLine.Trim());
                    currentLineStringBuilder.Append(desiredLineEnds);
                }
                
                trimmedString = currentLineStringBuilder.ToString();
            }

            return trimmedString;
        }

        public static string NormalizeLineEndings(this string currentString, String desiredLineEnd)
        {
            string normalizedString = currentString;

            if (!String.IsNullOrEmpty(currentString))
            {

                StringBuilder normalizedStringBuilder = new StringBuilder();

                String[] currentStringLines = 
                    currentString.Split(new string[] { "\r\n", "\n" },
                                        StringSplitOptions.RemoveEmptyEntries);

                foreach (String currentLine in currentStringLines)
                {
                    normalizedStringBuilder.Append(currentLine);
                    normalizedStringBuilder.Append(desiredLineEnd);
                }

                normalizedString = normalizedStringBuilder.ToString();
            }

            return normalizedString;
        }

        /// <summary>
        /// Gets the lower case letter of the alphabet corresponding to the index.
        /// e.g 1 = a, 26 = a
        /// </summary>
        /// <param name="alphabetIndex"></param>
        public static String GetLetterOfAlphabetByNumberLowerCase(int alphabetIndex)
        {
            //97 is a
            //122 is z           
            //Therefore, 96 is the offset to 'a' being 1
           
            string letterToReturn = "";

            if (alphabetIndex <= 26)
            {
                const int LOWER_CASE_ASCII_CODE_OFFSET = 96;

                int asciiCode = alphabetIndex + LOWER_CASE_ASCII_CODE_OFFSET;

                var alphabetCharacter = (char)asciiCode;
                letterToReturn = alphabetCharacter.ToString();
            }

            return letterToReturn;
        }

        /// <summary>
        /// Gets the upper case letter of the alphabet corresponding to the index.
        /// e.g 1 = a, 26 = a
        /// </summary>
        /// <param name="alphabetIndex"></param>
        public static String GetLetterOfAlphabetByNumberUpperCase(int alphabetIndex)
        {
            string alphabetCharacter = GetLetterOfAlphabetByNumberLowerCase(alphabetIndex);

            if (!String.IsNullOrWhiteSpace(alphabetCharacter))
            {
                alphabetCharacter = alphabetCharacter.ToUpper();
            }

            return alphabetCharacter;
        }

        /// <summary>
        /// Replaces Carriage Returns and Line Feeds with their escaped equivalents.
        /// </summary>
        /// <param name="currentString"></param>
        /// <returns></returns>
        public static string EscapeNewLines(this string currentString)
        {
            string escapedString = currentString;

            if (!String.IsNullOrWhiteSpace(currentString))
            {
                escapedString = 
                    currentString
                        .Replace("\r", "\\r")
                        .Replace("\n", "\\n");
            }


            return escapedString;
        }
    }
}